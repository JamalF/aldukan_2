import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[800],
      body: Padding(
        padding: const EdgeInsets.fromLTRB(0, 35, 0, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            FlatButton(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              onPressed: () {
                Navigator.pushNamed(context, '/dukan');
              },
              child: Image.asset('assets/dukan.jpg', height: 300, scale: 1,),
            ),
            Divider(thickness: 2.0,),
            Text(
              'الدكان',
              style: TextStyle(
                fontSize: 30.0,
              ),
            ),
            Divider(thickness: 2.0,),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  onPressed: () {
                    Navigator.pushNamed(context, '/customer');
                  },
                  child: Image.asset('assets/old_shopper.jpg', height: 300,scale: 1,),
                )],
            ),
          ],
        ),
      ),
    );
  }
}
