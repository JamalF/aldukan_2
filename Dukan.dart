import 'package:flutter/material.dart';

class Dukan extends StatefulWidget {
  @override
  _DukanState createState() => _DukanState();
}

class _DukanState extends State<Dukan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[800],
        centerTitle: true,
        title: Text('سجل'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'الاسم الاول: ',
                      labelStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 25,
                      ),
                      fillColor: Colors.amber,
                      filled: true,
                    ),
                  ),
                ),
              ),
            ),
            Directionality(
              textDirection: TextDirection.rtl,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'الاسم الأخير:  ',
                    labelStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                    ),
                    fillColor: Colors.amber,
                    filled: true,
                  ),
                ),
              ),
            ),
            Directionality(
              textDirection: TextDirection.rtl,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'اسم الدكانة: ',
                    labelStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                    ),
                    fillColor: Colors.amber,
                    filled: true,
                  ),
                ),
              ),
            ),
            Directionality(
              textDirection: TextDirection.rtl,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'موقع المتجر:  ',
                    labelStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                    ),
                    fillColor: Colors.amber,
                    filled: true,
                  ),
                ),
              ),
            ),
            Directionality(
              textDirection: TextDirection.rtl,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'ساعات العمل:  ',
                    labelStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                    ),
                    fillColor: Colors.amber,
                    filled: true,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
